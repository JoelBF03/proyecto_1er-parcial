/*----------------------------------
REGISTRO USUARIO
----------------------------------*/

//VALIDACIONES DE REGISTRO
function validarFormulario() {
    var nombre = document.getElementById("nombre").value;
    var apellidos = document.getElementById("apellidos").value;
    var cedula = document.getElementById("cedula").value;
    var email = document.getElementById("email").value;
    var contraseña = document.getElementById("contraseña-registro").value;
    var confirmarContraseña = document.getElementById("confirmar-contraseña-registro").value;
    
    // Validación de campos requeridos
    if (nombre === "" || apellidos === "" || cedula === "" || email === "" || contraseña === "" || confirmarContraseña === "" || facultad.value === "default" || carreras.value === "default") {
        alert("Por favor, complete todos los campos.");
        return false;
    }
    
    // Validación de nombres y apellidos
    if (nombre.length > 60 || apellidos.length > 60 || /\d/.test(nombre) || /\d/.test(apellidos)) {
        alert("Nombres y apellidos deben tener como máximo 60 caracteres y no pueden contener números.");
        return false;
    }
    
    // Validación de cédula
    if (!/^\d{1,10}$/.test(cedula)) {
        alert("La cédula debe contener solo números y tener como máximo 10 caracteres.");
        return false;
    }
    
    // Validación de correo electrónico
    if (!/^\S+@\S+\.\S+$/.test(email)) {
        alert("Ingrese un correo electrónico válido.");
        return false;
    }
    
    // Validación de contraseña
    if (!/(?=.*[A-Z])(?=.*\d)/.test(contraseña)) {
        alert("La contraseña debe contener al menos una letra en mayúscula y al menos un número.");
        return false;
    }
    
    // Confirmar contraseña
    if (contraseña !== confirmarContraseña) {
        alert("Las contraseñas no coinciden.");
        return false;
    }
    
    return true;
}

document.addEventListener('DOMContentLoaded', function() {

    //SELECCIONAR CARRERA DEPENDIENDO DE LA FACULTAD

    //asignar variables dependiendo de seleccion
    const seleccion_facultad = document.getElementById('facultad');
    const seleccion_carrera = document.getElementById('carreras');

    //Objeto que mapea carreras dependiendo de la facultad elegida
    const Facultad_Carrera = {
        v1: ["Medicina", "Odontología", "Enfermería", "Fisioterapia", "Fonoaudiología", "Laboratorio Clínico", "Terapia Ocupacional", "Psicología"],
        v2: ["Administración de Empresas", "Mercadotecnia", "Contabilidad y Auditoría", "Auditoria y Control de Gestión", "Finanzas", "Comercio Exterior", "Gestión de la Información Gerencial"],
        v3: ["Educación Inicial", "Educación Especial", "Psicología Educativa", "Educación Básica", "Pedagogía de la Actividad Física y el Deporte", "Pedagogía de la Lengua y la Literatura", "Pedagogía de los Idiomas Nacionales y Extranjeros", "Turismo", "Hospitalidad y Hotelería", "Artes Plásticas", "Sociología"],
        v4: ["Ingeniería Civil", "Ingeniería Marítima", "Electricidad", "Arquitectura", "Ingeniería Industrial", "Ingeniería de Alimentos"],
        v5: ["Ingeniería  Agropecuaria", "Agronegocios", "Ingeniería Agroindustrial", "Ingeniería Ambiental", "Ingeniería en Tecnologías de la Información", "Ingeniería en Software", "Ingeniería en Sistema", "Biología"],
        v6: ["Derecho", "Economía", "Trabajo Social", "Comunicación"]
    };

    //Agregar evento de seleccion
    seleccion_facultad.addEventListener('change', function() {
        const seleccion_final = seleccion_facultad.value;
        const elementos = Facultad_Carrera[seleccion_final];
        
        //Borrar carreras de facultad no elegida
        seleccion_carrera.innerHTML='';

        //Agregar opcion predeterminada
        const predeterminado = document.createElement('option');
        predeterminado.value = 'default';
        predeterminado.text = 'Selecciona una carrera';
        predeterminado.disabled = true;
        predeterminado.selected = true;
        seleccion_carrera.appendChild(predeterminado);

        //Agregar las opciones correspondientes a la carrera
        elementos.forEach(function(carreras) {
            const option = document.createElement('option');
            option.value = carreras;
            option.text = carreras;
            seleccion_carrera.appendChild(option);
        });
    });
});

/*----------------------------------
INICIAR SESION
----------------------------------*/

//Como no contamos con una base de datos, agregare un usuario por default
function validarInicioSesion() {
    const email_sesion = document.getElementById("email-sesion").value;
    const contraseña_sesion = document.getElementById("contraseña-sesion").value;
    if (email_sesion === "uleam@gmail.com" && contraseña_sesion === "Uleam123") {
        return true
    } else {
        alert("Credenciales incorrectas. Por favor, inténtalo de nuevo.");
        return false;
    }
}

//GUARDAR DATOS EN TABLA
function guardarRegistro() {
    const hora = document.getElementById("hora-asistencia").value;
    const fecha = document.getElementById("fecha-asistencia").value;

    // Comprobar si LocalStorage es compatible con el navegador
    if (typeof(Storage) !== "undefined") {
        
        // Obtener registros existentes o inicializar un array vacío
        let registros = JSON.parse(localStorage.getItem("registros")) || [];

        // Agregar el nuevo registro
        registros.push({ hora, fecha });

        // Almacenar la lista actualizada en LocalStorage
        localStorage.setItem("registros", JSON.stringify(registros));

        alert("Registro guardado con éxito.");
    } else {
        alert("Lo siento, tu navegador no soporta el almacenamiento local.");
    }
}

// MOSTRAR DATOS EN LA TABLA DEL HISTORIAL

document.addEventListener('DOMContentLoaded', function () {
    // Verificar si LocalStorage es compatible
    if (typeof(Storage) !== "undefined") {
        // Recuperar registros del almacenamiento local
        let registros = JSON.parse(localStorage.getItem("registros")) || [];

        const tabla = document.getElementById("tabla-asistencias");
        const cuerpoRegistros = document.getElementById("registro-asistencias");

        // Generar filas de la tabla para cada registro
        registros.forEach(function (registro) {
            const fila = document.createElement("tr");

            const columnaFecha = document.createElement("td");
            columnaFecha.textContent = registro.fecha;

            const columnaHora = document.createElement("td");
            columnaHora.textContent = registro.hora;

            const columnaDocente = document.createElement("td");
            columnaDocente.textContent = "Patricia Quiroz";

            const columnaAsistencia = document.createElement("td");
            columnaAsistencia.textContent = "Presente";

            fila.appendChild(columnaFecha);
            fila.appendChild(columnaHora);
            fila.appendChild(columnaDocente);
            fila.appendChild(columnaAsistencia);

            cuerpoRegistros.appendChild(fila);
        });
    } else {
        alert("Lo siento, tu navegador no soporta el almacenamiento local.");
    }
});

//CONSULTA

document.addEventListener('DOMContentLoaded', function () {
    if (typeof(Storage) !== "undefined") {
        const fechaConsultaInput = document.getElementById("fecha-consulta");
        const consultarButton = document.getElementById("consultar-button");
        const tablaConsulta = document.getElementById("tabla-consulta");
        const cuerpoConsulta = document.getElementById("cuerpo-consulta");

        // Manejar el evento click del botón de consulta
        consultarButton.addEventListener("click", function () {
            const fechaConsulta = fechaConsultaInput.value;

            // Recuperar registros del almacenamiento local
            let registros = JSON.parse(localStorage.getItem("registros")) || [];

            // Filtrar registros por fecha de consulta
            const registrosFiltrados = registros.filter(function (registro) {
                return registro.fecha === fechaConsulta;
            });

            // Limpiar la tabla de resultados
            cuerpoConsulta.innerHTML = "";

            // Generar filas de la tabla con los registros filtrados
            registrosFiltrados.forEach(function (registro) {
                const fila = document.createElement("tr");

                const columnaFecha = document.createElement("td");
                columnaFecha.textContent = registro.fecha;

                const columnaHora = document.createElement("td");
                columnaHora.textContent = registro.hora;

                const columnaDocente = document.createElement("td");
                columnaDocente.textContent = "ULEAM";

                const columnaAsistencia = document.createElement("td");
                columnaAsistencia.textContent = "Presente";

                fila.appendChild(columnaFecha);
                fila.appendChild(columnaHora);
                fila.appendChild(columnaDocente);
                fila.appendChild(columnaAsistencia);

                cuerpoConsulta.appendChild(fila);
            });
        });
    } else {
        alert("Lo siento, tu navegador no soporta el almacenamiento local.");
    }
});

